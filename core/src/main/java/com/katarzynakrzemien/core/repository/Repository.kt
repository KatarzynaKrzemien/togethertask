package com.katarzynakrzemien.core.repository

import com.katarzynakrzemien.core.model.*
import io.reactivex.Observable


interface Repository {
    fun organiser(): Observable<Organiser>
    fun users(): Observable<List<User>>
    fun addUserIfNotExist(user: User)
    fun addOrganiserIfNotExist(organiser: Organiser)
    fun addEvent(event: Event)
    fun addTask(task: Task)
    fun addProduct(product: Product)
    fun addShop(shop: Shop)
    fun deleteEvent(id: String)
    fun deleteTask(id: String)
    fun deleteProduct(id: String)
    fun deleteShop(id: String)
    fun updateEvent(event: Event)
    fun updateTask(task: Task)
    fun updateProduct(product: Product)
    fun updateShop(shop: Shop)

    fun refreshOrganiser()
}
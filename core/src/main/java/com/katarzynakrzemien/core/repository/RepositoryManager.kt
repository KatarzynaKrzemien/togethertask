package com.katarzynakrzemien.core.repository

interface RepositoryManager : Repository {
    fun cleanData()
}
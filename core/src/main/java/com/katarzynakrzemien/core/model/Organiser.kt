package com.katarzynakrzemien.core.model

import java.util.*

data class Organiser(
    override val id: String,
    val idUsers: List<String> = listOf(),
    val isAccept: Map<String, Boolean> = mapOf(),
    val shops: List<Shop> = listOf(),
    val products: List<Product> = listOf(),
    val tasks: List<Task> = listOf(),
    val events: List<Event> = listOf()
) : Item() {
    override fun toHashMap(): HashMap<String, Any> = hashMapOf("id" to id)
}
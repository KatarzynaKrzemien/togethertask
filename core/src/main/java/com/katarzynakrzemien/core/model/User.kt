package com.katarzynakrzemien.core.model

import java.util.*

data class User(
    override var id: String = defaultId(),
    var name: String = "",
    var mail: String = "",
    var members: List<Member> = listOf(),
    var color: String = ""
) : Item() {
    override fun toHashMap(): HashMap<String, Any> =
        hashMapOf("id" to id, "name" to name, "mail" to mail, "color" to color)
}

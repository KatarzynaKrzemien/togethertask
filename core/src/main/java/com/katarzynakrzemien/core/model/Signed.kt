package com.katarzynakrzemien.core.model

enum class Signed {
    IN,
    OUT
}
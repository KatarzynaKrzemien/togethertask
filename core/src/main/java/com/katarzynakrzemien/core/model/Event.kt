package com.katarzynakrzemien.core.model

import java.util.*

data class Event(
    override val id: String = defaultId(),
    val idGroup: String = defaultId(),
    val title: String = "",
    val idUsers: List<String> = listOf(),
    val category: Int = 0,
    val date: List<String> = listOf(),
    val isNotification: Boolean = false
) : Item() {
    override fun toHashMap(): HashMap<String, Any> = hashMapOf(
        "id" to id,
        "idGroup" to idGroup,
        "title" to title,
        "category" to category,
        "isNotification" to isNotification,
    )
}
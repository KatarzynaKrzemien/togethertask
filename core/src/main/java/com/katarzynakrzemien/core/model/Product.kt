package com.katarzynakrzemien.core.model

import java.util.*

data class Product(
    override val id: String = defaultId(),
    val name: String = "",
    val idShop: String = "",
    val nameShop: String = "",
    val isBought: Boolean = false,
    val idUnit: Int? = null,
    val amount: Int? = null
) : Item() {
    override fun toHashMap(): HashMap<String, Any> = hashMapOf(
        "id" to id,
        "name" to name,
        "idShop" to idShop,
        "isBought" to isBought
    )
}
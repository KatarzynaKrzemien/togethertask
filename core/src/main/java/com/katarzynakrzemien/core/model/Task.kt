package com.katarzynakrzemien.core.model

import java.util.*

data class Task(
    override val id: String = defaultId(),
    val content: String = "",
    val idUsers: List<String>? = null,
    val isDone: Boolean = false,
    val date: String? = null,
    val dateComplete: String? = null,
    val isNotification: Boolean = false
) : Item() {
    override fun toHashMap(): HashMap<String, Any> = hashMapOf(
        "id" to id,
        "content" to content,
        "isDone" to isDone,
        "isNotification" to isNotification
    )
}
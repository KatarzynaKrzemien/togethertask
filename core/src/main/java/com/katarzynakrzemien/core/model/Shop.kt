package com.katarzynakrzemien.core.model

import java.util.*

data class Shop(
    override val id: String = defaultId(),
    val name: String = ""
) : Item() {
    override fun toHashMap(): HashMap<String, Any> = hashMapOf("id" to id, "name" to name)
}
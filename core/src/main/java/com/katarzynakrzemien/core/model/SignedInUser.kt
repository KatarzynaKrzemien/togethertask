package com.katarzynakrzemien.core.model

object SignedInUser {
    var state = Signed.OUT
    var user: User = User()
        get() = if (state == Signed.IN) field else throw IllegalAccessError("User signed out")
        set(value) {
            field = value
            state = Signed.IN
        }

    fun signOut() {
        state = Signed.OUT
    }

    fun signIn(user: User) {
        state = Signed.IN
        SignedInUser.user = user
    }
}


package com.katarzynakrzemien.core.model

import java.util.*

abstract class Item(open val id: String = defaultId()) {

    abstract fun toHashMap(): HashMap<String, out Any>

    companion object {
        fun defaultId() = UUID.randomUUID().hashCode().toString()
    }
}

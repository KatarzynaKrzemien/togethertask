package com.katarzynakrzemien.domain.view

import com.katarzynakrzemien.domain.state.State

interface View<S : State> {
    fun render(state: S)
}

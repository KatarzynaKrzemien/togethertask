package com.katarzynakrzemien.domain.intent.organiser.settings.shops

import com.katarzynakrzemien.domain.intent.Intent

sealed class ShopsIntent : Intent
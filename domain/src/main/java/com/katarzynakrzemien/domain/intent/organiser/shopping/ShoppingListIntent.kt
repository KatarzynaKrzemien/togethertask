package com.katarzynakrzemien.domain.intent.organiser.shopping

import com.katarzynakrzemien.domain.intent.Intent

sealed class ShoppingListIntent : Intent
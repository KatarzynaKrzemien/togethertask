package com.katarzynakrzemien.domain.intent.organiser.calendar

import com.katarzynakrzemien.domain.intent.Intent

sealed class CalendarIntent : Intent
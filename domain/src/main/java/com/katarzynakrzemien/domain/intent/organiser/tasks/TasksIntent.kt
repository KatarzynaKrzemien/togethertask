package com.katarzynakrzemien.domain.intent.organiser.tasks

import com.katarzynakrzemien.domain.intent.Intent

sealed class TasksIntent : Intent
package com.katarzynakrzemien.domain.intent.organiser.settings

import com.katarzynakrzemien.domain.intent.Intent

sealed class SettingsIntent : Intent
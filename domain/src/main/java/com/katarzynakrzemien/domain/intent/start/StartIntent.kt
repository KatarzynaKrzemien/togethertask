package com.katarzynakrzemien.domain.intent.start

import com.katarzynakrzemien.domain.intent.Intent

sealed class StartIntent : Intent
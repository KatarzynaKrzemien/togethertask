package com.katarzynakrzemien.domain.intent.organiser.settings.notifications

import com.katarzynakrzemien.domain.intent.Intent

sealed class NotificationsIntent : Intent
package com.katarzynakrzemien.domain.viewmodel.organiser.tasks

import androidx.lifecycle.LiveData
import com.katarzynakrzemien.domain.intent.organiser.tasks.TasksIntent
import com.katarzynakrzemien.domain.state.organiser.tasks.TasksState
import com.katarzynakrzemien.domain.viewmodel.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel

@HiltViewModel
class TasksViewModel : BaseViewModel<TasksState, TasksIntent>() {
    override val intents: Channel<TasksIntent>
        get() = TODO("Not yet implemented")
    override val state: LiveData<TasksState>
        get() = TODO("Not yet implemented")
}
package com.katarzynakrzemien.domain.viewmodel.organiser.calendar

import androidx.lifecycle.LiveData
import com.katarzynakrzemien.domain.intent.organiser.calendar.CalendarIntent
import com.katarzynakrzemien.domain.state.organiser.calendar.CalendarState
import com.katarzynakrzemien.domain.viewmodel.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel

@HiltViewModel
class CalendarViewModel : BaseViewModel<CalendarState, CalendarIntent>() {
    override val intents: Channel<CalendarIntent>
        get() = TODO("Not yet implemented")
    override val state: LiveData<CalendarState>
        get() = TODO("Not yet implemented")
}
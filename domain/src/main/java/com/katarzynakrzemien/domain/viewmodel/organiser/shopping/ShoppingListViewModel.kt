package com.katarzynakrzemien.domain.viewmodel.organiser.shopping

import androidx.lifecycle.LiveData
import com.katarzynakrzemien.domain.intent.organiser.shopping.ShoppingListIntent
import com.katarzynakrzemien.domain.state.organiser.shopping.ShoppingListState
import com.katarzynakrzemien.domain.viewmodel.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel

@HiltViewModel
class ShoppingListViewModel : BaseViewModel<ShoppingListState, ShoppingListIntent>() {
    override val intents: Channel<ShoppingListIntent>
        get() = TODO("Not yet implemented")
    override val state: LiveData<ShoppingListState>
        get() = TODO("Not yet implemented")
}
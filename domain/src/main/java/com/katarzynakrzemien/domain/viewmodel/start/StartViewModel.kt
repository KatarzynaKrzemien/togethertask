package com.katarzynakrzemien.domain.viewmodel.start

import androidx.lifecycle.LiveData
import com.katarzynakrzemien.domain.intent.start.StartIntent
import com.katarzynakrzemien.domain.state.start.StartState
import com.katarzynakrzemien.domain.viewmodel.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel

@HiltViewModel
class StartViewModel : BaseViewModel<StartState, StartIntent>() {
    override val intents: Channel<StartIntent>
        get() = TODO("Not yet implemented")
    override val state: LiveData<StartState>
        get() = TODO("Not yet implemented")
}
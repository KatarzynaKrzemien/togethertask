package com.katarzynakrzemien.domain.viewmodel.organiser.settings

import androidx.lifecycle.LiveData
import com.katarzynakrzemien.domain.intent.organiser.settings.SettingsIntent
import com.katarzynakrzemien.domain.state.organiser.settings.SettingsState
import com.katarzynakrzemien.domain.viewmodel.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel

@HiltViewModel
class SettingsViewModel : BaseViewModel<SettingsState, SettingsIntent>() {
    override val state: LiveData<SettingsState>
        get() = TODO("Not yet implemented")
    override val intents: Channel<SettingsIntent>
        get() = TODO("Not yet implemented")
}
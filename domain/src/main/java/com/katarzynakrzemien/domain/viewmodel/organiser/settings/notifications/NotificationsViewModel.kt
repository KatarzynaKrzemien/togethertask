package com.katarzynakrzemien.domain.viewmodel.organiser.settings.notifications

import androidx.lifecycle.LiveData
import com.katarzynakrzemien.domain.intent.organiser.settings.notifications.NotificationsIntent
import com.katarzynakrzemien.domain.state.organiser.settings.notifications.NotificationsState
import com.katarzynakrzemien.domain.viewmodel.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel

@HiltViewModel
class NotificationsViewModel : BaseViewModel<NotificationsState, NotificationsIntent>() {
    override val state: LiveData<NotificationsState>
        get() = TODO("Not yet implemented")
    override val intents: Channel<NotificationsIntent>
        get() = TODO("Not yet implemented")
}
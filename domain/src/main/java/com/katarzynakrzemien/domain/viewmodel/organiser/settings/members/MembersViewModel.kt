package com.katarzynakrzemien.domain.viewmodel.organiser.settings.members

import androidx.lifecycle.LiveData
import com.katarzynakrzemien.domain.intent.organiser.settings.members.MembersIntent
import com.katarzynakrzemien.domain.state.organiser.settings.members.MembersState
import com.katarzynakrzemien.domain.viewmodel.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel

@HiltViewModel
class MembersViewModel : BaseViewModel<MembersState, MembersIntent>() {
    override val state: LiveData<MembersState>
        get() = TODO("Not yet implemented")
    override val intents: Channel<MembersIntent>
        get() = TODO("Not yet implemented")
}
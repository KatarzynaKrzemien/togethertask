package com.katarzynakrzemien.domain.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.katarzynakrzemien.domain.intent.Intent
import com.katarzynakrzemien.domain.state.State
import kotlinx.coroutines.channels.Channel

abstract class BaseViewModel<S : State, I : Intent> : ViewModel() {
    abstract val intents: Channel<I>
    abstract val state: LiveData<S>
}
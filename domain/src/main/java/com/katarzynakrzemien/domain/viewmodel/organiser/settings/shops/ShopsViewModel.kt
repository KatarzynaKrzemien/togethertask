package com.katarzynakrzemien.domain.viewmodel.organiser.settings.shops

import androidx.lifecycle.LiveData
import com.katarzynakrzemien.domain.intent.organiser.settings.shops.ShopsIntent
import com.katarzynakrzemien.domain.state.organiser.settings.shops.ShopsState
import com.katarzynakrzemien.domain.viewmodel.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel

@HiltViewModel
class ShopsViewModel : BaseViewModel<ShopsState, ShopsIntent>() {
    override val state: LiveData<ShopsState>
        get() = TODO("Not yet implemented")
    override val intents: Channel<ShopsIntent>
        get() = TODO("Not yet implemented")
}
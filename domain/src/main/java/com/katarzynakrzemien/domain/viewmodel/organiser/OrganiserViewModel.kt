package com.katarzynakrzemien.domain.viewmodel.organiser

import androidx.lifecycle.LiveData
import com.katarzynakrzemien.domain.intent.organiser.OrganiserIntent
import com.katarzynakrzemien.domain.state.organiser.OrganiserState
import com.katarzynakrzemien.domain.viewmodel.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel

@HiltViewModel
class OrganiserViewModel : BaseViewModel<OrganiserState, OrganiserIntent>() {
    override val state: LiveData<OrganiserState>
        get() = TODO("Not yet implemented")
    override val intents: Channel<OrganiserIntent>
        get() = TODO("Not yet implemented")
}
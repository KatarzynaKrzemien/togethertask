package com.katarzynakrzemien.domain.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.katarzynakrzemien.domain.viewmodel.organiser.OrganiserViewModel
import com.katarzynakrzemien.domain.viewmodel.organiser.calendar.CalendarViewModel
import com.katarzynakrzemien.domain.viewmodel.organiser.settings.SettingsViewModel
import com.katarzynakrzemien.domain.viewmodel.organiser.shopping.ShoppingListViewModel
import com.katarzynakrzemien.domain.viewmodel.organiser.tasks.TasksViewModel
import com.katarzynakrzemien.domain.viewmodel.start.StartViewModel

class ViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T = when {
        modelClass.isAssignableFrom(StartViewModel::class.java) -> StartViewModel()
        modelClass.isAssignableFrom(OrganiserViewModel::class.java) -> OrganiserViewModel()
        modelClass.isAssignableFrom(CalendarViewModel::class.java) -> CalendarViewModel()
        modelClass.isAssignableFrom(TasksViewModel::class.java) -> TasksViewModel()
        modelClass.isAssignableFrom(ShoppingListViewModel::class.java) -> ShoppingListViewModel()
        modelClass.isAssignableFrom(SettingsViewModel::class.java) -> SettingsViewModel()
        else -> throw IllegalArgumentException("Missing class $modelClass. Add $modelClass to ${this.javaClass}")
    } as T
}
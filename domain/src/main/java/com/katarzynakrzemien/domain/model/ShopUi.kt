package com.katarzynakrzemien.domain.model

data class ShopUi(
    override val id: String,
    val name: String
) : ItemUi
package com.katarzynakrzemien.domain.model.transformer

import com.katarzynakrzemien.core.model.*
import com.katarzynakrzemien.domain.model.*
import com.katarzynakrzemien.domain.model.UserUi.Companion.COLOR
import com.katarzynakrzemien.domain.model.UserUi.Companion.COLOR.USER_1
import com.katarzynakrzemien.domain.model.UserUi.Companion.COLOR.USER_2

object Transformer {

    fun transform(item: Event, users: List<User>): EventUi {
        val usersId = users.map { it.id }
        return EventUi(
            id = item.id,
            idGroup = item.idGroup,
            title = item.title,
            users = item.idUsers.map { transform(users[usersId.indexOf(it)]) },
            category = item.category,
            date = item.date.map { DateUi(it) },
            isNotification = item.isNotification
        )
    }

    fun transform(uiItem: EventUi) = Event(
        id = uiItem.id,
        idGroup = uiItem.idGroup,
        title = uiItem.title,
        idUsers = uiItem.users.map { it.id },
        category = uiItem.category,
        date = uiItem.date.map { it.time },
        isNotification = uiItem.isNotification
    )

    fun transform(item: Organiser, users: List<User>): OrganiserUi {
        val usersId = users.map { it.id }
        return OrganiserUi(
            id = item.id,
            users = item.idUsers.map { transform(users[usersId.indexOf(it)]) },
            isAccept = item.isAccept,
            shops = item.shops.map { transform(it) },
            products = item.products.map { transform(it) },
            tasks = item.tasks.map { transform(it, users) },
            events = item.events.map { transform(it, users) }
        )
    }

    fun transform(uiItem: OrganiserUi) = Organiser(
        id = uiItem.id,
        idUsers = uiItem.users.map { it.id },
        isAccept = uiItem.isAccept,
        shops = uiItem.shops.map { transform(it) },
        products = uiItem.products.map { transform(it) },
        tasks = uiItem.tasks.map { transform(it) },
        events = uiItem.events.map { transform(it) }
    )

    fun transform(item: Product) = ProductUi(
        id = item.id,
        name = item.name,
        shop = transform(Shop(item.idShop, item.nameShop)),
        isBought = item.isBought,
        unit = item.idUnit?.let { transform(it) },
        amount = item.amount
    )

    fun transform(unit: Int) =
        when (unit) {
            UNIT.KG.id -> UNIT.KG
            UNIT.DAG.id -> UNIT.DAG
            UNIT.G.id -> UNIT.G
            UNIT.L.id -> UNIT.L
            UNIT.ITEM.id -> UNIT.ITEM
            else -> throw IllegalArgumentException("Unknown unit type. Add type with id $unit to ${UNIT::class.java}")
        }

    fun transform(uiItem: ProductUi) = Product(
        id = uiItem.id,
        name = uiItem.name,
        idShop = uiItem.shop.id,
        nameShop = uiItem.shop.name,
        isBought = uiItem.isBought,
        idUnit = uiItem.unit?.id,
        amount = uiItem.amount
    )

    fun transform(item: Shop) = ShopUi(item.id, item.name)

    fun transform(uiItem: ShopUi) = Shop(uiItem.id, uiItem.name)

    fun transform(item: Task, users: List<User>): TaskUi {
        val usersId = users.map { it.id }
        return TaskUi(
            id = item.id,
            content = item.content,
            users = item.idUsers?.map { transform(users[usersId.indexOf(it)]) },
            isDone = item.isDone,
            date = item.date?.let { DateUi(it) },
            dateComplete = item.dateComplete?.let { DateUi(it) },
            isNotification = item.isNotification
        )
    }

    fun transform(uiItem: TaskUi) = Task(
        id = uiItem.id,
        content = uiItem.content,
        idUsers = uiItem.users?.map { it.id },
        isDone = uiItem.isDone,
        date = uiItem.date?.time,
        dateComplete = uiItem.dateComplete?.time,
        isNotification = uiItem.isNotification
    )

    fun transform(item: User) = UserUi(
        id = item.id,
        name = item.name,
        mail = item.mail,
        members = item.members.map { transform(it) },
        color = transform(item.color)
    )

    fun transform(uiItem: UserUi): User = User(
        id = uiItem.id,
        name = uiItem.name,
        mail = uiItem.mail,
        members = uiItem.members.map { transform(it) },
        color = uiItem.color.id
    )

    fun transform(item: Member) = MemberUi(
        id = item.id,
        name = item.name,
        mail = item.mail,
        color = transform(item.color)
    )

    fun transform(uiItem: MemberUi) = Member(
        id = uiItem.id,
        name = uiItem.name,
        mail = uiItem.mail,
        color = uiItem.color.id
    )

    fun transform(idColor: String) =
        when (idColor) {
            USER_1.id -> USER_1
            USER_2.id -> USER_2
            else -> throw IllegalArgumentException("Unknown unit type. Add type with id $idColor to ${COLOR::class.java}")
        }

}
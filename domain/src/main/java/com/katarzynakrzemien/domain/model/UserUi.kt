package com.katarzynakrzemien.domain.model

import android.graphics.Color

data class UserUi(
    override val id: String,
    val name: String,
    val mail: String,
    val members: List<MemberUi>,
    val color: COLOR
) : ItemUi {

    companion object {
        enum class COLOR(val id: String) {
            USER_1("1"),
            USER_2("2")
        }
    }
}

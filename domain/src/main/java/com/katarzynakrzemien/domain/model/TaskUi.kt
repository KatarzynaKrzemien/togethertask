package com.katarzynakrzemien.domain.model

data class TaskUi(
    override val id: String,
    val content: String,
    val users: List<UserUi>?,
    val isDone: Boolean,
    val date: DateUi?,
    val dateComplete: DateUi?,
    val isNotification: Boolean
) : ItemUi
package com.katarzynakrzemien.domain.model

data class OrganiserUi(
    override val id: String,
    val users: List<UserUi>,
    val isAccept: Map<String, Boolean>,
    val shops: List<ShopUi>,
    val products: List<ProductUi>,
    val tasks: List<TaskUi>,
    val events: List<EventUi>
) : ItemUi
package com.katarzynakrzemien.domain.model

data class EventUi(
    override val id: String,
    val idGroup: String,
    val title: String,
    val users: List<UserUi>,
    val category: Int,
    val date: List<DateUi>,
    val isNotification: Boolean
) : ItemUi

enum class CATEGORY {
    BIRTHDAY,
    ANNIVERSARY,
    FRIENDS,
    FAMILY,
    SHOPPING,
    PARTY,
    DOGS,
    RPG,
    DOCTOR,
    WORK,
    EXAM,
    TRAVEL,
    DATE,
    CLEANING,
    DINNER,
    MOVIE,
    FREE_TIME,
    SURPRISE
}
package com.katarzynakrzemien.domain.model

import java.text.SimpleDateFormat
import java.util.*

data class DateUi(val time: String) {

    val year: Int = if (time.isNotEmpty()) time.split(split)[2].toInt() else 0
    val month: Int = if (time.isNotEmpty()) time.split(split)[1].toInt().minus(1) else 0
    val day: Int = if (time.isNotEmpty()) time.split(split)[0].toInt() else 0

    companion object {
        private const val split = "."
        fun createDate(
            day: Int = Calendar.getInstance().get(Calendar.DAY_OF_MONTH),
            month: Int = Calendar.getInstance().get(Calendar.MONTH),
            year: Int = Calendar.getInstance().get(Calendar.YEAR)
        ): String =
            SimpleDateFormat("dd${split}MM${split}yyyy", Locale.getDefault()).format(
                Calendar.getInstance().apply {
                    set(Calendar.DAY_OF_MONTH, day)
                    set(Calendar.MONTH, month)
                    set(Calendar.YEAR, year)
                }.timeInMillis
            )
    }
}
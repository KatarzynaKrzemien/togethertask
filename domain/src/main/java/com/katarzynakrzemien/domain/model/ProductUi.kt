package com.katarzynakrzemien.domain.model

data class ProductUi(
    override val id: String,
    val name: String,
    val shop: ShopUi,
    val isBought: Boolean,
    val unit: UNIT?,
    val amount: Int?
) : ItemUi

enum class UNIT(val id: Int) {
    KG(1),
    G(2),
    DAG(3),
    L(4),
    ITEM(5)
}
package com.katarzynakrzemien.domain.model

data class MemberUi(
    override val id: String,
    val name: String,
    var mail: String,
    val color: UserUi.Companion.COLOR
) : ItemUi
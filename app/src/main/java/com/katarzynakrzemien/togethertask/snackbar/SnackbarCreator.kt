package com.katarzynakrzemien.togethertask.snackbar

import android.content.Context
import android.view.View
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.katarzynakrzemien.togethertask.R
import com.katarzynakrzemien.togethertask.extension.getColorFromAttr


object SnackbarCreator {

    fun makeAndShowLightSnackBar(
        context: Context,
        view: View,
        message: String,
        actionText: Int? = null,
        action: (() -> Unit)? = null
    ) = makeAndShowSnackbar(context, view, message, context.getColorFromAttr(R.attr.textColorDark), actionText, action)
        .apply {
            this.view.setBackgroundColor(context.getColorFromAttr(R.attr.snackbarLightBackground))
        }
        .show()

    fun makeAndShowDarkSnackbar(
        context: Context,
        view: View,
        message: String,
        actionText: Int? = null,
        action: (() -> Unit)? = null
    ) = makeAndShowSnackbar(context, view, message, context.getColorFromAttr(R.attr.textColorLight), actionText, action)
        .apply {
            this.view.background =
                ContextCompat.getDrawable(context, R.drawable.ic_launcher_background)
        }
        .show()

    private fun makeAndShowSnackbar(
        context: Context,
        view: View,
        message: String,
        textColor: Int,
        actionText: Int? = null,
        action: (() -> Unit)? = null
    ): Snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG)
        .setTextColor( textColor)
        .setAction(actionText?.let { context.getString(it) }) { action?.invoke() }

}
package com.katarzynakrzemien.togethertask.repository

import com.katarzynakrzemien.core.model.*
import com.katarzynakrzemien.core.repository.Repository
import com.katarzynakrzemien.core.repository.RepositoryManager
import io.reactivex.Observable

class LocalRepositoryManager(private val repository: Repository) : RepositoryManager {

    private var organiser: Observable<Organiser>? = null
    private var users: Observable<List<User>>? = null

    override fun cleanData() {
        users = null
        organiser = null
    }

    override fun organiser(): Observable<Organiser> {
        if (organiser == null) organiser = repository.organiser()
        return organiser!!
    }

    override fun users(): Observable<List<User>> {
        if (users == null) users = repository.users()
        return users!!
    }

    override fun addUserIfNotExist(user: User) = repository.addUserIfNotExist(user)

    override fun addOrganiserIfNotExist(organiser: Organiser) =
        repository.addOrganiserIfNotExist(organiser)

    override fun addEvent(event: Event) = repository.addEvent(event)

    override fun addTask(task: Task) = repository.addTask(task)

    override fun addProduct(product: Product) = repository.addProduct(product)

    override fun addShop(shop: Shop) = repository.addShop(shop)

    override fun deleteEvent(id: String) = repository.deleteEvent(id)

    override fun deleteTask(id: String) = repository.deleteTask(id)

    override fun deleteProduct(id: String) = repository.deleteProduct(id)

    override fun deleteShop(id: String) = repository.deleteShop(id)

    override fun updateEvent(event: Event) = repository.updateEvent(event)

    override fun updateTask(task: Task) = repository.updateTask(task)

    override fun updateProduct(product: Product) = repository.updateProduct(product)

    override fun updateShop(shop: Shop) = repository.updateShop(shop)

    override fun refreshOrganiser() = repository.refreshOrganiser()
}
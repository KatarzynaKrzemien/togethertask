package com.katarzynakrzemien.togethertask.repository

import com.katarzynakrzemien.core.model.*
import com.katarzynakrzemien.core.repository.Repository
import io.reactivex.Observable

class FirebaseRepository : Repository {
    override fun organiser(): Observable<Organiser> {
        TODO("Not yet implemented")
    }

    override fun users(): Observable<List<User>> {
        TODO("Not yet implemented")
    }

    override fun addUserIfNotExist(user: User) {
        TODO("Not yet implemented")
    }

    override fun addOrganiserIfNotExist(organiser: Organiser) {
        TODO("Not yet implemented")
    }

    override fun addEvent(event: Event) {
        TODO("Not yet implemented")
    }

    override fun addTask(task: Task) {
        TODO("Not yet implemented")
    }

    override fun addProduct(product: Product) {
        TODO("Not yet implemented")
    }

    override fun addShop(shop: Shop) {
        TODO("Not yet implemented")
    }

    override fun deleteEvent(id: String) {
        TODO("Not yet implemented")
    }

    override fun deleteTask(id: String) {
        TODO("Not yet implemented")
    }

    override fun deleteProduct(id: String) {
        TODO("Not yet implemented")
    }

    override fun deleteShop(id: String) {
        TODO("Not yet implemented")
    }

    override fun updateEvent(event: Event) {
        TODO("Not yet implemented")
    }

    override fun updateTask(task: Task) {
        TODO("Not yet implemented")
    }

    override fun updateProduct(product: Product) {
        TODO("Not yet implemented")
    }

    override fun updateShop(shop: Shop) {
        TODO("Not yet implemented")
    }

    override fun refreshOrganiser() {
        TODO("Not yet implemented")
    }
}
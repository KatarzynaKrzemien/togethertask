package com.katarzynakrzemien.togethertask.ui.organiser

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.katarzynakrzemien.domain.state.organiser.OrganiserState
import com.katarzynakrzemien.domain.viewmodel.organiser.OrganiserViewModel
import com.katarzynakrzemien.togethertask.R
import com.katarzynakrzemien.togethertask.extension.getColorFromAttr
import com.katarzynakrzemien.togethertask.extension.getIdNavDestinationSet
import com.katarzynakrzemien.togethertask.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_organiser.*


class OrganiserActivity : BaseActivity<OrganiserViewModel, OrganiserState>() {

    override val layoutRes: Int = R.layout.activity_organiser
    override val viewModel: OrganiserViewModel by viewModels()
    private lateinit var navController: NavController

    private val navigationListener =
        NavController.OnDestinationChangedListener { _, navDestination: NavDestination, _ ->
            if (navDestination.id == R.id.settingsFragment) {
                adjustView(
                    R.drawable.ic_logout,
                    getColorFromAttr(R.attr.user_one), //todo sign in user color
                    getColorFromAttr(R.attr.textColorLight),
                    false
                )
            } else {
                adjustView(
                    R.drawable.ic_add,
                    getColorFromAttr(android.R.attr.windowBackground),
                    getColorFromAttr(R.attr.textColorDark),
                    true
                )
            }
        }

    private fun adjustView(
        @DrawableRes fabIcon: Int,
        toolbarBackgroundColor: Int,
        toolbarTextColor: Int,
        isLightStatusBar: Boolean
    ) {
        fab.setImageDrawable(ContextCompat.getDrawable(this, fabIcon))
        toolbar.apply {
            setBackgroundColor(toolbarBackgroundColor)
            setTitleTextColor(toolbarTextColor)
        }
        window.apply {
            decorView.apply {
               // TODO isLightStatusBar
                /* systemUiVisibility =
                     if (isLightStatusBar) {
                         systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                     } else {
                         View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                     }*/
            }
            statusBarColor = toolbarBackgroundColor
        }
    }


    override fun render(state: OrganiserState) {
        TODO("Not yet implemented")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupGraphNav()
    }

    private fun setupGraphNav() {
        with(findNavController(R.id.navigationHost)) {
            navController = this
            bottomNavigationView.setupWithNavController(this)
            bottomNavigationView.menu.getItem(2).isEnabled = false
            AppBarConfiguration(this.graph)
            setSupportActionBar(toolbar.apply {
                setupWithNavController(
                    navController,
                    AppBarConfiguration(navController.graph.getIdNavDestinationSet())
                )
            })
        }
    }

    override fun onPause() {
        navController.removeOnDestinationChangedListener(navigationListener)
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        navController.addOnDestinationChangedListener(navigationListener)
    }

    companion object {
        fun getIntent(context: Context): Intent = Intent(context, OrganiserActivity::class.java)
    }

}
package com.katarzynakrzemien.togethertask.ui.organiser.settings.notification

import androidx.fragment.app.viewModels
import com.katarzynakrzemien.domain.state.organiser.settings.notifications.NotificationsState
import com.katarzynakrzemien.domain.viewmodel.organiser.settings.notifications.NotificationsViewModel
import com.katarzynakrzemien.togethertask.R
import com.katarzynakrzemien.togethertask.ui.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NotificationsFragment : BaseFragment<NotificationsViewModel, NotificationsState>() {

    override val layoutRes: Int = R.layout.fragment_notifications
    override val viewModel: NotificationsViewModel by viewModels()

    override fun render(state: NotificationsState) {
        TODO("Not yet implemented")
    }
}
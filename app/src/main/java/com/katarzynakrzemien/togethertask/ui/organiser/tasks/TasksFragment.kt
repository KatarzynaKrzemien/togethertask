package com.katarzynakrzemien.togethertask.ui.organiser.tasks

import androidx.fragment.app.viewModels
import com.katarzynakrzemien.domain.state.organiser.tasks.TasksState
import com.katarzynakrzemien.domain.viewmodel.organiser.tasks.TasksViewModel
import com.katarzynakrzemien.togethertask.R
import com.katarzynakrzemien.togethertask.ui.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TasksFragment : BaseFragment<TasksViewModel, TasksState>() {

    override val layoutRes: Int = R.layout.fragment_tasks
    override val viewModel: TasksViewModel by viewModels()

    override fun render(state: TasksState) {
        TODO("Not yet implemented")
    }

}
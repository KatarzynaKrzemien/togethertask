package com.katarzynakrzemien.togethertask.ui.organiser.settings.members

import androidx.fragment.app.viewModels
import com.katarzynakrzemien.domain.state.organiser.settings.members.MembersState
import com.katarzynakrzemien.domain.viewmodel.organiser.settings.members.MembersViewModel
import com.katarzynakrzemien.togethertask.R
import com.katarzynakrzemien.togethertask.ui.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MembersFragment : BaseFragment<MembersViewModel, MembersState>() {

    override val layoutRes: Int = R.layout.fragment_members
    override val viewModel: MembersViewModel by viewModels()

    override fun render(state: MembersState) {
        TODO("Not yet implemented")
    }
}
package com.katarzynakrzemien.togethertask.ui.organiser.settings

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.katarzynakrzemien.togethertask.ui.organiser.settings.SettingsFragment.Companion.TAB_COUNT
import com.katarzynakrzemien.togethertask.ui.organiser.settings.members.MembersFragment
import com.katarzynakrzemien.togethertask.ui.organiser.settings.notification.NotificationsFragment
import com.katarzynakrzemien.togethertask.ui.organiser.settings.shops.ShopsFragment

class SettingsAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int = TAB_COUNT

    override fun createFragment(position: Int): Fragment =
        when (position) {
            0 -> MembersFragment()
            1 -> ShopsFragment()
            2 -> NotificationsFragment()
            else -> throw IllegalArgumentException("Unknown position $position. Add the position to  createFragment method in ${this::class.java}")
        }
}
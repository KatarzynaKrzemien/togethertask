package com.katarzynakrzemien.togethertask.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import com.katarzynakrzemien.domain.state.State
import com.katarzynakrzemien.domain.view.View

abstract class BaseActivity<T : ViewModel, S : State> : AppCompatActivity(), View<S> {

    protected abstract val layoutRes: Int
    protected abstract val viewModel: T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutRes)
    }

}
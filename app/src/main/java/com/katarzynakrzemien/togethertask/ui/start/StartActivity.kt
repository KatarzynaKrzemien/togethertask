package com.katarzynakrzemien.togethertask.ui.start

import android.content.Context
import android.content.Intent
import android.view.View
import android.view.View.VISIBLE
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.katarzynakrzemien.domain.state.start.StartState
import com.katarzynakrzemien.domain.viewmodel.start.StartViewModel
import com.katarzynakrzemien.togethertask.R
import com.katarzynakrzemien.togethertask.snackbar.SnackbarCreator
import com.katarzynakrzemien.togethertask.ui.BaseActivity
import com.katarzynakrzemien.togethertask.ui.organiser.OrganiserActivity
import kotlinx.android.synthetic.main.activity_start.*

class StartActivity : BaseActivity<StartViewModel, StartState>() {

    override val layoutRes: Int = R.layout.activity_start
    override val viewModel: StartViewModel by viewModels()

    private var signOut: Boolean = false
    private val googleClient: GoogleSignInClient by lazy {
        GoogleSignIn.getClient(
            this,
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()
        )
    }
    private val startActivityResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            handleSignInResult(GoogleSignIn.getSignedInAccountFromIntent(result.data))
        }

    override fun render(state: StartState) {}

    private fun signIn() {
        startActivityResult.launch(googleClient.signInIntent)
    }

    private fun handleSignInResult(task: Task<GoogleSignInAccount>) {
        try {
            task.getResult(ApiException::class.java)?.let(::setSignedInUser)
        } catch (e: ApiException) {
            SnackbarCreator.makeAndShowLightSnackBar(
                this,
                container,
                getString(R.string.something_wrong)
            )
        }
    }

    private fun setSignedInUser(account: GoogleSignInAccount) {
        //TODO log/add user to repo
        startActivity(OrganiserActivity.getIntent(this))
        finish()
    }

    override fun onStart() {
        super.onStart()
        GoogleSignIn.getLastSignedInAccount(this)?.let { setSignedInUser(it) }
    }

    override fun onResume() {
        super.onResume()
        showViews()
        googleSignIn.setOnClickListener { signIn() }
    }

    private fun showViews() {
        showViewWithAnim(
            googleSignIn,
            AnimationUtils.loadAnimation(this, R.anim.slide_bottom_in_long)
        )
        with(AnimationUtils.loadAnimation(this, R.anim.logo_show)) {
            showViewWithAnim(signIn, this)
            showViewWithAnim(logo, this)
        }
    }

    private fun showViewWithAnim(view: View, animation: Animation) {
        view.startAnimation(animation)
        view.visibility = VISIBLE
    }

    companion object {
        const val KEY_SIGN_OUT = "sign_out"
        fun getIntent(context: Context, signOut: Boolean = false): Intent =
            Intent(context, StartActivity::class.java).apply {
                putExtra(KEY_SIGN_OUT, signOut)
            }
    }
}
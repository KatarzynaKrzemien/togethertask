package com.katarzynakrzemien.togethertask.ui.organiser.shopping

import androidx.fragment.app.viewModels
import com.katarzynakrzemien.domain.state.organiser.shopping.ShoppingListState
import com.katarzynakrzemien.domain.viewmodel.organiser.settings.SettingsViewModel
import com.katarzynakrzemien.domain.viewmodel.organiser.shopping.ShoppingListViewModel
import com.katarzynakrzemien.togethertask.R
import com.katarzynakrzemien.togethertask.ui.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ShoppingListFragment : BaseFragment<ShoppingListViewModel,ShoppingListState>() {

    override val layoutRes: Int = R.layout.fragment_shopping_list
    override val viewModel: ShoppingListViewModel by viewModels()

    override fun render(state: ShoppingListState) {
        TODO("Not yet implemented")
    }

}
package com.katarzynakrzemien.togethertask.ui.organiser.settings

import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import androidx.fragment.app.viewModels
import androidx.viewpager2.widget.ViewPager2
import com.katarzynakrzemien.domain.state.organiser.settings.SettingsState
import com.katarzynakrzemien.domain.viewmodel.organiser.settings.SettingsViewModel
import com.katarzynakrzemien.togethertask.R
import com.katarzynakrzemien.togethertask.extension.getColorFromAttr
import com.katarzynakrzemien.togethertask.extension.setBackgroundTint
import com.katarzynakrzemien.togethertask.extension.setTint
import com.katarzynakrzemien.togethertask.ui.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_settings.*

@AndroidEntryPoint
class SettingsFragment : BaseFragment<SettingsViewModel, SettingsState>() {

    override val layoutRes: Int = R.layout.fragment_settings
    override val viewModel: SettingsViewModel by viewModels()
    private lateinit var adapter: SettingsAdapter
    private var iconLightColor: Int? = null
    private var iconDarkColor: Int? = null

    override fun render(state: SettingsState) {
        TODO("Not yet implemented")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = SettingsAdapter(this)
        viewPager.adapter = adapter
        setupTabs()
        setupViewPager()
    }

    private fun setupTabs() {
        prepareTabsColors()
        setOnClickTabListener(0)
        setOnClickTabListener(1)
        setOnClickTabListener(2)
    }

    private fun setOnClickTabListener(position: Int) {
        getTabView(position).setOnClickListener {
            selectTab(position)
            viewPager.setCurrentItem(position, true)
        }
    }

    private fun setupViewPager() {
        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                selectTab(position)
            }
        })
    }

    private fun selectTab(position: Int) {
        clearTabs()
        setupTab(getTabView(position), true)
    }

    private fun clearTabs() {
        setupTab(members, false)
        setupTab(shops, false)
        setupTab(notification, false)
    }

    private fun setupTab(tab: ImageButton, isSelected: Boolean) {
        iconLightColor?.let { light ->
            iconDarkColor?.let { dark ->
                tab.apply {
                    if (isSelected) {
                        setBackgroundTint(dark)
                        setTint(light)
                    } else {
                        setBackgroundTint(light)
                        setTint(dark)
                    }
                }
            }
        }
    }

    private fun getTabView(position: Int) =
        when (position) {
            0 -> members
            1 -> shops
            2 -> notification
            else -> throw IllegalArgumentException("Unknown position $position. Add position to ${this::class.java}")
        }

    private fun prepareTabsColors() {
        context?.let {
            iconDarkColor = it.getColorFromAttr(R.attr.iconColorDark)
            iconLightColor = it.getColorFromAttr(R.attr.iconColorLight)
        }
    }

    companion object {
        const val TAB_COUNT = 3
    }

}
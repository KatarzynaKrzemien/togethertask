package com.katarzynakrzemien.togethertask.ui.organiser.calendar

import androidx.fragment.app.viewModels
import com.katarzynakrzemien.domain.state.organiser.calendar.CalendarState
import com.katarzynakrzemien.domain.viewmodel.organiser.calendar.CalendarViewModel
import com.katarzynakrzemien.togethertask.R
import com.katarzynakrzemien.togethertask.ui.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CalendarFragment : BaseFragment<CalendarViewModel, CalendarState>() {

    override val layoutRes: Int = R.layout.fragment_calendar
    override val viewModel: CalendarViewModel by viewModels()

    override fun render(state: CalendarState) {
        TODO("Not yet implemented")
    }
}
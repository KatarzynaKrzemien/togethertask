package com.katarzynakrzemien.togethertask.ui.splash

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.katarzynakrzemien.togethertask.ui.start.StartActivity


class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startActivity(StartActivity.getIntent(this))
        finish()
    }
}
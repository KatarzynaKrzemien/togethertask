package com.katarzynakrzemien.togethertask.ui.organiser.settings.shops

import androidx.fragment.app.viewModels
import com.katarzynakrzemien.domain.state.organiser.settings.shops.ShopsState
import com.katarzynakrzemien.domain.viewmodel.organiser.settings.shops.ShopsViewModel
import com.katarzynakrzemien.togethertask.R
import com.katarzynakrzemien.togethertask.ui.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ShopsFragment : BaseFragment<ShopsViewModel, ShopsState>() {

    override val layoutRes: Int = R.layout.fragment_shops
    override val viewModel: ShopsViewModel by viewModels()

    override fun render(state: ShopsState) {
        TODO("Not yet implemented")
    }
}
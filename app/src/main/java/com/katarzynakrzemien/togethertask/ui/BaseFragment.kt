package com.katarzynakrzemien.togethertask.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.katarzynakrzemien.domain.state.State

abstract class BaseFragment<T : ViewModel, S : State> : Fragment(),
    com.katarzynakrzemien.domain.view.View<S> {

    protected abstract val layoutRes: Int
    protected abstract val viewModel: T

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(layoutRes, container, false)
}
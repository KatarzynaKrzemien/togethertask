package com.katarzynakrzemien.togethertask.extension

import android.content.res.ColorStateList
import android.widget.ImageButton

fun ImageButton.setBackgroundTint(color: Int) {
    backgroundTintList = ColorStateList.valueOf(color)
}

fun ImageButton.setTint(color: Int) {
    setColorFilter(color, android.graphics.PorterDuff.Mode.MULTIPLY)
}


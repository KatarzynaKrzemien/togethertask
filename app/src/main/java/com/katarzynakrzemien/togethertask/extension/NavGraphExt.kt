package com.katarzynakrzemien.togethertask.extension

import androidx.navigation.NavGraph

fun NavGraph.getIdNavDestinationSet() = this.distinct().map { it.id }.toSet()